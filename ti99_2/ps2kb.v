//
// PS/2 to TI-99/2 keyboard matrix converter
//
// This source code is public domain
//


// PS/2 KBD interface (input only)
//
// Algorithm based on a VHDL routine by Grant Searle
//
module PS2KBD(
  input wire clk,
  
  input wire ps2_clk,
  input wire ps2_data,
  
  output reg [7:0] ps2_code,
  output reg strobe,
  output reg err
);

  // sync ps2_data
  //
  reg serin;
  always @(posedge clk) serin <= ps2_data;
  
  // sync & 'debounce' ps2_clock
  //
  parameter LEN = 8;
  reg bitclk = 0;
  reg [LEN:0] stable = 0;

  always @(posedge clk)
  begin
    stable = { stable[LEN-1:0], ps2_clk };
    if ( &stable) bitclk <= 1;
    if (~|stable) bitclk <= 0;
  end
  
  wire bitedge = bitclk && (~|stable[LEN-1:0]);
  
  // clock in KBD bits (start - 8 data - odd parity - stop)
  //
  reg [8:0] shift = 0;
  reg [3:0] bitcnt = 0;
  reg parity = 0;

  always @(posedge clk)
  begin
    strobe <= 0; err <= 0;
    if (bitedge) begin
      // wait for start bit
      if (bitcnt==0) begin
        parity <= 0;
        if (!serin) bitcnt <= 1;
        end
      // shift in 9 bits (8 data + parity)
      else if (bitcnt<10) begin
        shift  <= { serin, shift[8:1] };
        parity <= parity ^ serin;
        bitcnt <= bitcnt + 1;
        end
      // check stop bit, parity
      else begin
        bitcnt <= 0;
        if (parity && serin) begin
          ps2_code <= shift[7:0];
          strobe <= 1;
          end
        else
          err <= 1;
      end
    end
  end

endmodule

// PS/2 to TI-99/2 matrix Keyboard encoder
//
module ps2matrix(
  input wire clk,
  input wire ps2clk, ps2data,

  input wire [5:0] col,
  input wire [2:0] row,
  output reg out
);

  wire [7:0] code;
  wire strobe;
  PS2KBD kdb(clk, ps2clk, ps2data, code, strobe, );

  reg [7:0] matrix[5:0];
  reg extended = 0, action = 0;

  initial begin
    matrix[0] = 8'hff; matrix[1] = 8'hff; matrix[2] = 8'hff;
    matrix[3] = 8'hff; matrix[4] = 8'hff; matrix[5] = 8'hff;
  end

  // Debug outputs
  //
  reg special = 0;
  wire shifted = !matrix[4][7] | !matrix[5][1];

  // Read out KBD matrix
  //
  reg [7:0] line;
  always @(*)
  begin
    line = 8'hff;
    if (!col[0]) line = line & matrix[0];
    if (!col[1]) line = line & matrix[1];
    if (!col[2]) line = line & matrix[2];
    if (!col[3]) line = line & matrix[3];
    if (!col[4]) line = line & matrix[4];
    if (!col[5]) line = line & matrix[5];
    if (!col[5] && row==1 && special) line[1] = 0;
    out <= line[row];
  end

  // Convert PS/2 scan codes into TI99/2 KBD matrix state
  //
  reg [7:0] decode;

  always @(posedge clk)
  begin
    if (strobe) begin
      if (code==8'he0)
        extended <= 1;
      else if (code==8'hf0)
        action <= 1; // up
      else begin
        extended <= 0;
        action <= 0; // down

/* Convenient key remaps:
  \       => fctn-z
  `       => fctn-c
  shift-` => fctn-w (~)
*/
        // Convenience mappings
        decode = code;
        special <= !action;
        if (!shifted) begin
            case (code)
            8'h4e: decode = 8'h4a; // -     -> shift-/
            8'h52: decode = 8'h44; // '     -> shift-O
            8'h54: decode = 8'h2d; // [     -> shift-R
            8'h5b: decode = 8'h2c; // ]     -> shift-T
            8'h6b: decode = 8'h1b; // left  -> shift-S
            8'h74: decode = 8'h23; // right -> shift-D
            8'h75: decode = 8'h24; // up    -> shift-E
            8'h72: decode = 8'h22; // down  -> shift-X
            default: special <= 0;
          endcase
          end
        else begin
          case (code)
            8'h4e: decode = 8'h3c; // _     -> shift-U
            8'h4a: decode = 8'h43; // ?     -> shift-I
            8'h52: decode = 8'h4d; // "     -> shift-P
            8'h54: decode = 8'h2b; // {     -> shift-F
            8'h5b: decode = 8'h34; // }     -> shift-G
            default: special <= 0;
          endcase
        end

        case (decode)

          8'h16: matrix[0][7] <= action; // 1
          8'h1e: matrix[0][6] <= action; // 2
          8'h26: matrix[0][5] <= action; // 3
          8'h25: matrix[0][4] <= action; // 4
          8'h2e: matrix[0][3] <= action; // 5			
          8'h36: matrix[0][2] <= action; // 6
          8'h3d: matrix[0][1] <= action; // 7
          8'h3e: matrix[0][0] <= action; // 8
          8'h46: matrix[1][0] <= action; // 9
          8'h45: matrix[2][0] <= action; // 0
      //  8'h4e: // -
          8'h55: matrix[3][0] <= action; // =
      //  8'h66: // Backspace

      //  8'h0d: // TAB
          8'h15: matrix[1][7] <= action; // Q
          8'h1d: matrix[1][6] <= action; // W
          8'h24: matrix[1][5] <= action; // E
          8'h2d: matrix[1][4] <= action; // R
          8'h2c: matrix[1][3] <= action; // T				
          8'h35: matrix[1][2] <= action; // Y
          8'h3c: matrix[2][2] <= action; // U
          8'h43: matrix[1][1] <= action; // I
          8'h44: matrix[2][1] <= action; // O
          8'h4d: matrix[3][1] <= action; // P
      //  8'h54: // [
      //  8'h5b: // ]
      //  8'h0e: // Backslash

      //  8'h58: // Caps lock
          8'h1c: matrix[2][7] <= action; // A
          8'h1b: matrix[2][6] <= action; // S
          8'h23: matrix[2][5] <= action; // D
          8'h2b: matrix[2][4] <= action; // F
          8'h34: matrix[3][4] <= action; // G
          8'h33: matrix[2][3] <= action; // H
          8'h3b: matrix[3][3] <= action; // J
          8'h42: matrix[3][2] <= action; // K
          8'h4b: matrix[4][2] <= action; // L
          8'h4c: matrix[4][1] <= action; // ;
      //  8'h52: // '
          8'h5a: matrix[5][0] <= action; // ENTER

          8'h12: matrix[4][7] <= action; // L-SHIFT
          8'h1a: matrix[3][7] <= action; // Z
          8'h22: matrix[3][6] <= action; // X
          8'h21: matrix[3][5] <= action; // C
          8'h2a: matrix[4][5] <= action; // V
          8'h32: matrix[5][5] <= action; // B
          8'h31: matrix[4][4] <= action; // N
          8'h3a: matrix[5][4] <= action; // M
          8'h41: matrix[4][3] <= action; // ,
          8'h49: matrix[5][3] <= action; // .
          8'h4a: matrix[4][0] <= action; // /
          8'h59: matrix[5][1] <= action; // R-SHIFT

          8'h76: matrix[5][7] <= action; // ESC = BREAK
          8'h29: matrix[5][6] <= action; // SPACE
          8'h14: matrix[4][6] <= action; // CTRL
          8'h11: matrix[5][2] <= action; // L-ALT = FCTN
        endcase
      end
    end
  end

endmodule
