// TI99/2 Video Chip
//
module vdp99_2(
  input  wire pixclk,              // pixel clock (25Mhz)

  output vde, hsyn, vsyn, video,   // B/W video out with VGA frame timing

  output wire [15:0] ab,           // address out, valid when vma==1
  input  wire  [7:0] db_in,        // data in
  
  output      vma,                 // address valid
  output wire hold,                // hold CPU during video output
  input  wire viden,               // video enable
  output wire vint                 // vertical sync interrupt
);

  // VGA-resolution sync frame
  //
  reg [9:0] hctr = 0, vctr = 524;
  
  wire vde  = (hctr<640) && (vctr<480);
  wire hsyn = (hctr>655) && (hctr<752);
  wire vsyn = (vctr>489) && (vctr<492);
  
  assign vint = (vctr==492) && (hctr==0);
  
  always @(posedge pixclk) hctr <= (hctr==799) ? 0 : hctr + 1;
  always @(posedge pixclk) if(hctr==799) vctr <= (vctr==524) ? 0 :  vctr + 1;

  // Address multiplexer ('pat' selects pattern address)
  //
  reg pat = 0;
  reg vma = 0;

  assign ab = pat ? 16'h1c00 | { data[6:0], vctr[3:1] }
                  : 16'hec00 | { vctr[8:4], hctr[8:4] };
  
  // Controller FSM
  //
  wire aa = (hctr<512) && (vctr<384);               // active area
  wire eh = (hctr>794) && (vctr<384 || vctr==524);  // early hold
  wire cl = (vctr==384);                            // ctrl byte line

  reg [2:0] state = 0;
  
  localparam ab_data = 3'd0, get_data = 3'd1, ab_pat = 3'd2,
             get_pat = 3'd3, sh_wait  = 3'd4, skipwt = 3'd5;

  assign hold = (aa|eh|cl) && (state!=skipwt); // hold CPU during VDP access period

  reg [7:0] data = 0, ctrl = 6;
  reg ld_shift = 0;
  
  always @(posedge pixclk)
  begin
    pat <= 0; vma <= 0; ld_shift <= 0;
    if (!viden)
      state <= skipwt;
    else if (aa) begin
      case (state)
      ab_data:  begin vma <= 1; state <= get_data; end
      get_data: begin vma <= 1; state <= ab_pat; end
      ab_pat:   begin pat <= 1; vma <= 1; data <= db_in; state <= (db_in[7]) ? skipwt : get_pat; end
      get_pat:  begin pat <= 1; vma <= 1; ld_shift <= 1; state <= sh_wait; end
      sh_wait:  begin state <= (reload) ? ab_data : sh_wait; end
      skipwt:   /* do nothing, wait for end of line active area */;
      endcase
      end
    else begin
      state <= ab_data; // prepare for next scan line
      if (cl) begin
        case (hctr)
          0:  begin vma  <= 1;     end // ab = control address
          1:  begin vma  <= 1;     end
          2:  begin ctrl <= db_in; end // get control byte
          default: state <= skipwt;    // release CPU for rest of line
        endcase
      end
    end      
  end

  // Shift counter (start next load after 10 VGA pixels)
  //
  reg [3:0] shc = 0;
  
  always @(posedge pixclk) shc <= ld_shift ? 0 : shc + 1;

  wire reload = (shc==10);
  
  // Shift the 8 pattern bits into 16 VGA pixels
  //
  reg [7:0] shr = 0;
  always @(posedge pixclk)
  begin
    if (ld_shift) begin
      shr <= ctrl[2] ? db_in : ~db_in;
    end
    else if (!hctr[0]) begin
      shr <= { shr[6:0], !ctrl[2] };
    end
  end

  // Delay aa by 5 pixels to get the correct border cut off
  reg [4:0] daa = 0;
  always @(posedge pixclk) daa <= { daa[3:0], aa };
  wire border = !daa[4];
  
  wire video = border ? ctrl[1] : shr[7];

endmodule
