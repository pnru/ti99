**TI-99/2 in Verilog**

The TI-99/2 is a never released home computer in the TI-99/4A family. Only a few pre-production units remain, and some documentation. This is an implementation of the TI-99/2 on the ULX3S fpga board.

Output is to the GDPI port and sends an HDMI compatible video stream (VGA resolution). Input is through a PS/2 keyboard (or a PS/2 capable USB keyboard) hooked up to the USB2 port. Cassette I/O and the HEXBUS interface are not implemented. The implementation is of the 32KB ROM version of TI-99/2. The system has 32KB RAM.

The CPU implements the 9900 instruction set, plus two extensions from the 9995 set (LST, LWP). The databus has been kept at 16 bits wide.

Documentation for the TI-99/2 can be found here:
http://ftp.whtech.com/datasheets%20and%20manuals/99-2%20Computer/

A review of the surviving engineering samples can be found here:
https://translate.google.com/translate?hl=en&sl=auto&tl=en&u=http%3A%2F%2Fwww.ti99.com%2Fti%2Findex.php%3Farticle7%2Fbasic-computer-99-2

(original French language page: http://www.ti99.com/ti/index.php?article7/basic-computer-99-2)

The code also implements the HEXBUS interface. It talks to the ESP32 module on the ULX3S board and this module provides emulation of the HEXBUS disk drive. It could in principle also provide emulation of other HEXBUS devices (RS232, printer, etc.)

The TI-99/2 is the simplest member of the TI-99 family. The next step up would probably be adding a 9918 VDP chip in Verilog and creating a Tomy Tutor on FPGA.
