//
// Simplistic 16384x16 RAM module
//
// This source code is public domain
//

module RAM(
  input wire CLK,
  input wire nCS,
  input wire nWE,
  input wire [13:0] ADDR,
  input wire [15:0] DI,
  output reg [15:0] DO
);
  
  // Implementation
  reg [15:0] mem[0:16383];
  
  always @(posedge CLK)
  begin
    if (!nCS) begin
      if (!nWE) mem[ADDR] <= DI;
    end
  end

  always @(posedge CLK)
  begin
    if (!nCS) begin
      DO <= mem[ADDR];
    end
  end

`ifdef __ICARUS__
  initial begin : prefill
    integer i;
    for(i=0; i<16384; i=i+1) mem[i] = 0;
  end
`endif
 
endmodule
