// TI-99/2 HEXBUS module
//
// This source code is plublic domain
//
// For HEXBUS documentation see:
//    http://ftp.whtech.com/hexbus_cc40_ti74/
//
module HEXBUS_992 (
  input  wire clk,
  input  wire sel,
  input  wire [2:0] ab,
  input  wire cruout,
  output wire cruin,
  input  wire cruclk,
  output wire tx,           // data to device UART
  input  wire rx,           // data from device UART
  output wire ncts,         // ncts to device UART
  output wire [7:0] dbg
);

  // Controller CRU input & output bits
  //
  reg  [7:0] hbout = 8'hff;
  wire [7:0] hbin;

  // output
  always @(negedge cruclk) begin
    if (sel) hbout[ab] = cruout;
  end
  
  wire [3:0] host_do = hbout[3:0];
  wire hsk_cpu       = hbout[4];
  wire bav_cpu       = hbout[5];

  // input
  assign cruin = hbin[ab];
  
  assign hbin[3:0] = host_di;
  assign hbin[4]   = hsk_cmb;
  assign hbin[5]   = bav_cmb;
  assign hbin[6]   = !hsk_latch;
  assign hbin[7]   = 1; // cassette in

  // HSK host latch
  //
  reg  hsk_cmb1, hsk_latch = 1;
  
  // detect falling edge on combined bus HSK
  always @(posedge clk) hsk_cmb1 <= hsk_cmb;
  wire hsk_edge = hsk_cmb1 && !hsk_cmb;
  
  // latch logic
  always @(posedge clk)
  begin
    if (hsk_edge && hsk_cpu) hsk_latch <= 0;
    if ((!hsk_cpu)) hsk_latch <= 1;
  end

  // Combine all bus signals (open collector 'and')
  //
  wire hsk_cmb = hsk_cpu & hsk_latch & hsk_snd & hsk_rcv;
  wire bav_cmb = bav_cpu;
  
  // In- and outbound HEXBUS <-> SERIAL bridge
  //
  wire hsk_snd, hsk_rcv, cmd_mode;
  wire [3:0] host_di;

  HEXBUS_SND bridge_out(clk, host_do, hsk_cmb, bav_cpu, hsk_snd, cmd_mode, tx, dbg);
  HEXBUS_RCV bridge_in (clk, host_di, hsk_cmb, bav_cpu, hsk_rcv, cmd_mode, rx, ncts);

endmodule

// HEXBUS slave for sending to serial (to ESP32 or a PC)
//
// Serial protocol:
// - 1000000 bps, 8N1
// - Each two nibbles are sent as a byte
// - The start of a message (BAV goes low) marker 0xc1 is sent
// - The end of a message (BAV goes high) marker 0xc2 is sent
// - Last nibble of command is not acknowldged until confirmation from device
//   is received (cmd_mode goes low)
//
module HEXBUS_SND (
  input  wire clk,              // clock
  input  wire [3:0] hexbus_do,  // hexbus out from host
  input  wire hsk_in,           // hsk
  input  wire bav,              // bav
  output wire hsk_out,          // hsk hold
  input  wire cmd_mode,         // 1 = processing command, 0 = processing reply
  output wire host_tx,          // host hexbus uart out
  output wire [7:0] dbg
);

  assign dbg = msg_ctr[7:0];

  // ignore hexbus nibbles coming from the RCV unit
  wire hsk = hsk_in | !cmd_mode;

  // transmit state machine
  //
  localparam wt_hsk1 = 4'b0000, wt_hsk2 = 4'b0001, wt_hsk3 = 4'b0010, wt_hsk4 = 4'b0011,
             wt_tx1  = 4'b0100, wt_tx2  = 4'b0101, wt_tx3  = 4'b0110, do_opn  = 4'b0111, // these 4 hold HSK low
             upper4  = 4'b1000, lower4  = 4'b1001, do_esc  = 4'b1010, wt_hsk5 = 4'b1011,
             wt_tx4  = 4'b1100, do_cls  = 4'b1101, wt_bav  = 4'b1110; // these 3 states are okay when BAV is high

  reg [3:0] tx_state = wt_bav, tx_state_d;
   
  // timer for 1,000,000 baud @ 25 MHz clock // 217
  //
  reg [7:0] tx_timer = 0;
  wire xbit_clk = !(|tx_timer);

  always @(posedge clk) begin
    if (sig_ld)
      tx_timer <= 1;
    else if (tx_timer==217)
      tx_timer <= 0;
    else
      tx_timer <= tx_timer + 1;
  end
  
  // transmit shift register
  //   10 bits to hold start-byte-stop
  //
  reg [9:0] host_xmit = -1, host_xmit_d;
  
  always @(posedge clk) host_xmit <= host_xmit_d;

  always @(*)
  begin
    case (tx_state)
    upper4:  host_xmit_d = { 1'd1, data,  1'd0};    // send data byte
    do_opn:  host_xmit_d = { 1'd1, 8'hc1, 1'd0};    // send open code:  0xc1
    do_cls:  host_xmit_d = { 1'd1, 8'hc2, 1'd0};    // send close code: 0xc2
    default:
      host_xmit_d = xbit_clk ? { 1'd1, host_xmit[9:1] } : host_xmit;  // shift next bit
    endcase
  end
  
  assign host_tx = host_xmit_d[0];

  // transmit counter, counts out 10 bits, then idle (ctr==0)
  //
  reg [3:0] tx_ctr = 0, tx_ctr_d;
  reg sig_ld;
  
  always @(posedge clk) tx_ctr <= tx_ctr_d;
  
  always @(*)
  begin
    tx_ctr_d = tx_ctr;
    case (tx_state)
      upper4, do_opn, do_cls, do_esc: begin
        tx_ctr_d = 10;
        sig_ld = 1;
        end
      default: begin
        if (xbit_clk) tx_ctr_d = xmit_free ? 0 : tx_ctr - 1;
        sig_ld = 0;
        end
    endcase
  end
  
  wire xmit_free = !(|tx_ctr);

  // Assemble two nibbles into a data byte
  //
  reg [3:0] lower;
  always @(posedge clk) if (tx_state==lower4) lower <= hexbus_do;
  wire [7:0] data = {hexbus_do, lower};

  // Device ready logic
  //   Count out the message header bytes, and latch in the msg data size
  //   Count out the msg data bytes and do not acknowledge last nibble
  //   until a device ready confirmation has been received (cmd_mode low).
  //
  reg  [3:0] hdr_ctr = 0;
  reg [15:0] msg_ctr = 0;
  
  always @(posedge clk)
  begin
    if (bav) begin
      hdr_ctr <= 0;
      msg_ctr <= 0;
      end
    else if (tx_state==upper4) begin
      if (hdr_ctr!=10)
        hdr_ctr <= hdr_ctr + 1;
      case (hdr_ctr)
      default: /* do nothing */;
       7: msg_ctr[ 7:0] <= {hexbus_do, lower};
       8: msg_ctr[15:8] <= {hexbus_do, lower};
      10: msg_ctr <= msg_ctr - 1;
      endcase
    end
  end
  
  // Signal receipt of last command byte. Cater to both commands with data
  // block (msg_ctr==1) and without data block (msg_ctr==0). In both cases
  // wait until receipt of the 2nd nibble
  wire last_cmd_byte = (msg_ctr==1 || (msg_ctr==0 && hdr_ctr==9)) && (tx_state==wt_hsk5);

  // Slave pulls HSK low:
  // - in any of the wt_tx1..3 + do_opn states
  // - for as long as remote device is preparing reply (to avoid bus timeout)
  assign hsk_out = !(tx_state[3:2]==2'b01) & !(last_cmd_byte & cmd_mode) & !(tx_state==upper4);

  // HOST -> DEVICE state machine transistions
  //
  always @(posedge clk) tx_state <= tx_state_d;
  
  always @(*)
  begin
    case (tx_state)
    wt_bav:  tx_state_d = bav ? wt_bav : wt_hsk1;           // wait for BAV to go low
    wt_hsk1: tx_state_d = hsk ? wt_hsk1 : wt_tx1;           // wait for HSK to go low
    wt_tx1:  tx_state_d = xmit_free ? do_opn : wt_tx1;      // wait for tx_uart free; hold HSK
    do_opn:  tx_state_d = wt_tx2;                           // send msg open sequence; hold HSK

    wt_hsk2: tx_state_d = hsk ? wt_hsk2 : wt_tx2;           // wait for HSK to go low
    wt_tx2:  tx_state_d = xmit_free ? lower4 : wt_tx2;      // wait for tx_uart free; hold HSK
    lower4:  tx_state_d = wt_hsk3;                          // latch lower nibble;
    wt_hsk3: tx_state_d = hsk ? wt_hsk4 : wt_hsk3;          // wait for HSK to go high
    wt_hsk4: tx_state_d = hsk ? wt_hsk4 : wt_tx3;           // wait for HSK to go low again
    wt_tx3:  tx_state_d = xmit_free ? upper4 : wt_tx3;      // wait for tx_uart free; hold HSK; next send byte
    upper4:  tx_state_d = wt_hsk5;                          // latch upper nibble & start tx_uart
    wt_hsk5: tx_state_d = hsk ? wt_hsk2 : wt_hsk5;          // wait for HSK to go high & handle next nibble

    wt_tx4:  tx_state_d = xmit_free ? do_cls : wt_tx4;      // wait for tx_uart free;
    do_cls:  tx_state_d = wt_bav;                           // send msg close sequence and wait for next msg
    default: tx_state_d = wt_bav;
    endcase
    
    if (bav && tx_state[3:2]!=2'b11) tx_state_d = wt_tx4;   // BAV high forces end of message
  end
  
`ifdef __ICARUS__
/* DEBUG */
always @(posedge clk) begin
if (tx_state==upper4) $display("->%h", data);
end
`endif

endmodule

// HEXBUS slave for sending to serial (to ESP32 or a PC)
//
// Serial protocol:
// - 1000000 bps, 8N1
// - Each byte received is passed on as two nibbles
// - Device transmission is throttled via its CTS input
// - Switches from command to reply mode upond receipt of 0xc3
//
module HEXBUS_RCV(
  input  wire clk,             // 25 mhx clock
  output wire [3:0] host_di,   // hexbus D0..D3
  input  wire hsk_cmb,         // HSK combined
  input  wire bav,             // BAV
  output wire hsk_out,         // HSK drive
  output      cmd_mode,        // 1 = processing command, 0 = processing reply
  input  wire host_rx,         // serial input from device
  output wire dev_ncts         // cts to device
//  output wire [7:0] dbg
);

//  assign dbg = data;

  // Bit timer for 1,000,000 baud @ 25 MHz clock
  //
  reg [7:0] rx_timer = 0;

  wire rbit_clk = !(|rx_timer);
  reg ld_rx_tmr;

  always @(posedge clk) begin
    if (ld_rx_tmr)
      rx_timer <= 325; // 1.5 bit time
    else if (rbit_clk)
      rx_timer <= 217; // 1 bit time 
    else
      rx_timer <= rx_timer - 1;
  end

  reg rx;
  always @(posedge clk) rx = host_rx;

  // Receiver side of uart (start - 8 data - stop)
  //
  reg [7:0] shift  = 0, data;
  reg [3:0] bitcnt = 0;
  reg strobe = 0;

  always @(posedge clk)
  begin
    strobe <= 0; ld_rx_tmr <= 0;
    if (bitcnt==0) begin
      // wait for start bit
      if (!rx) begin
        bitcnt <= 1;
        ld_rx_tmr <= 1;
        end
      end
    else if (rbit_clk) begin
      // shift in 8 bits
`ifdef __ICARUS__
      if (bitcnt<9) begin
`else
      if (bitcnt<10) begin // !! FIXME - seems bug in Yosys
`endif
        shift  <= { rx, shift[7:1] };
        bitcnt <= bitcnt + 1;
        end
      // latch byte
      else begin
        bitcnt <= 0;
        strobe <= 1;
        data <= shift;
      end
    end
  end

  // Receiver state machine
  //
  reg [3:0] rx_state = 0;
  
  localparam  wt_stb  = 4'b0000, lower4 = 4'b0001, wt_lwr  = 4'b0010, wt_hsk1 = 4'b0011,
              upper4  = 4'b0100, wt_upr = 4'b0101, wt_hsk2 = 4'b0110, wt_mid1 = 4'b0111,
              wt_mid2 = 4'b1000;

  // Hold further serial input as long as current byte is not yet accepted by host
  assign dev_ncts = !(rx_state==wt_stb);
  
  // Move to receive mode when device is ready
  wire dev_rdy = (cmd_mode && data==8'hc3);
  reg  cmd_mode;
  
  always @(posedge clk)
  begin
    if (dev_rdy) cmd_mode <= 0;
    if (bav)     cmd_mode <= 1;
  end

  // Timer to stretch outboud HSK low to 1 us
  reg [7:0] hsk_tmr = 0;
  
  always @(posedge clk)
  begin
    if (rx_state==lower4 || rx_state==upper4)
      hsk_tmr <= 25;
    else if (!hsk_out)
      hsk_tmr <= hsk_tmr - 1;
  end

  // Timer to separate nibbles by 8 us minimum
  reg [7:0] hsk_sep = 0;
  
  always @(posedge clk)
  begin
    if (rx_state==wt_mid1)
      hsk_sep <= 200;
    else
      hsk_sep <= hsk_sep - 1;
  end

  wire hsk_next = !(|hsk_sep);
  
  // DEVICE -> HOST state machine transitions
  //
  wire start = strobe & !cmd_mode;

  always @(posedge clk)
  begin
    case (rx_state)
    wt_stb:  rx_state <= start ? lower4 : wt_stb;     // wait for byte to arrive
    lower4:  rx_state <= wt_lwr;                      // start lower nibble
    wt_lwr:  rx_state <= hsk_out ? wt_hsk1 : wt_lwr;  // wait 1 us
    wt_hsk1: rx_state <= hsk_cmb ? wt_mid1 : wt_hsk1; // wait for HSK to go high
    wt_mid1: rx_state <= wt_mid2;                     // start timer
    wt_mid2: rx_state <= hsk_next ? upper4 : wt_mid2; // wait 8 us between nibbles
    upper4:  rx_state <= wt_hsk2;                     // start upper nibble
    wt_upr:  rx_state <= hsk_out ? wt_hsk2 : wt_upr;  // wait 1 us
    wt_hsk2: rx_state <= hsk_cmb ? wt_stb : wt_hsk2;  // wait for HSK to go high
    default: rx_state <= wt_stb;
    endcase
    if (bav) rx_state <= wt_stb;
  end
  
  // Outbound HEXBUS signals
  assign hsk_out  = !(|hsk_tmr);
  assign host_di  = (rx_state==wt_lwr || rx_state==wt_hsk1) ? data[3:0] :
                    (rx_state==wt_upr || rx_state==wt_hsk2) ? data[7:4] :
                    4'hf;

`ifdef __ICARUS__
/* DEBUG */
always @(posedge clk) begin
if (rx_state==upper4) $display("<-%h", data);
end
`endif

endmodule
