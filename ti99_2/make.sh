#!/bin/sh

# Build TI-99/2 system for ULX3S board

# synthesise design
# generates warnings, but these are okay
yosys -q -p "synth_ecp5 -json vdp.json" tiram.v tirom.v evmvdp.v tms99000.v ps2kb.v vdp99_2.v hdmi.v hexbus.v

# place & route
# assumes 25F device
nextpnr-ecp5 --25k --package CABGA381 --json vdp.json --lpf ulx3s.lpf --textcfg vdp.cfg

# pack bitstream
# idcode only needed when sending bitstream to 12F devices
ecppack  vdp.cfg vdp.bit --idcode 0x21111043

# send to ULX3S board (store in configuration RAM)
ujprog vdp.bit
