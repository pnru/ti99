`timescale 1ns/1ns

module vdp_tb;

  // 25MHz bit clock
  reg clk_25mhz = 0;
  always #20 clk_25mhz <= !clk_25mhz;
  
  reg reset, int = 0;
  reg ps2clk = 1, ps2data = 1;

  sys tut(clk_25mhz, , , , , ps2clk, ps2data, , , reset, int);

  initial begin
    $dumpfile("test.vcd");
    $dumpvars(0,vdp_tb);

    reset = 1;
    #500
    reset = 0;
    
    send(8'h1e); // key '2' down
    #2380000
    int = 1;
    #50
    int = 0;
    #31000
    int = 1;
    #50
    int = 0;

    send(8'hf0); // key '2' up
    send(8'h1e);
    send(8'h16); // key '1' down
    #1200000
    int = 1;
    #50
    int = 0;
    #31000
    int = 1;
    #50
    int = 0;    

    #12000000
    $write("\n");
    $finish;
  end
  
  task send;
    input  [7:0]  char;
  
    integer i;
    reg par;
    begin
      ps2data = 0;
      #400 ps2clk = 0;
      par = 0;
      for(i=0; i<8; i=i+1) begin
        #400 ps2clk = 1;
        ps2data = char[i];
        par = par ^ ps2data;
        #400 ps2clk = 0;
      end
      #400 ps2clk = 1;
      ps2data = !par;
      #400 ps2clk = 0;
      #400 ps2clk = 1;
      ps2data = 1;
      #400 ps2clk = 0;
      #400 ps2clk = 1;
      #800;
    end
  endtask
  
endmodule
