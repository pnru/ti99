//
// Simplistic 16384x16 ROM module
//
// This source code is public domain
//

module ROM(
  input wire CLK,
  input wire nCS,
  input wire [13:0] ADDR,
  output reg [15:0] DO
);
  
  // Implementation
  reg [15:0] mem[0:16383];
  
  always @(posedge CLK)
  begin
    if (!nCS) begin
      DO <= mem[ADDR];
    end
  end
  
  initial
  begin
    $readmemh("rom/rom992.mem", mem); // 32KB rom set
  end
  
endmodule